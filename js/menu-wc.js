'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">co-co-a-front documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-c747a3d25c579a61283e577684fb5088"' : 'data-target="#xs-components-links-module-AppModule-c747a3d25c579a61283e577684fb5088"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-c747a3d25c579a61283e577684fb5088"' :
                                            'id="xs-components-links-module-AppModule-c747a3d25c579a61283e577684fb5088"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConsultasModule.html" data-type="entity-link">ConsultasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConsultasModule-289c9ba9495ccee3dfe230ef5502a90b"' : 'data-target="#xs-components-links-module-ConsultasModule-289c9ba9495ccee3dfe230ef5502a90b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConsultasModule-289c9ba9495ccee3dfe230ef5502a90b"' :
                                            'id="xs-components-links-module-ConsultasModule-289c9ba9495ccee3dfe230ef5502a90b"' }>
                                            <li class="link">
                                                <a href="components/ConsultasAddComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConsultasAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConsultasHomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConsultasHomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConsultasTablaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConsultasTablaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CuadroMandoModule.html" data-type="entity-link">CuadroMandoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CuadroMandoModule-900c58544a804d4747965a2f4d45e9b1"' : 'data-target="#xs-components-links-module-CuadroMandoModule-900c58544a804d4747965a2f4d45e9b1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CuadroMandoModule-900c58544a804d4747965a2f4d45e9b1"' :
                                            'id="xs-components-links-module-CuadroMandoModule-900c58544a804d4747965a2f4d45e9b1"' }>
                                            <li class="link">
                                                <a href="components/CuadroMandoHomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CuadroMandoHomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-95d7bac32b357accb5aa9e39cae122f1"' : 'data-target="#xs-components-links-module-HomeModule-95d7bac32b357accb5aa9e39cae122f1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-95d7bac32b357accb5aa9e39cae122f1"' :
                                            'id="xs-components-links-module-HomeModule-95d7bac32b357accb5aa9e39cae122f1"' }>
                                            <li class="link">
                                                <a href="components/InicioComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InicioComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LayOutModule.html" data-type="entity-link">LayOutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LayOutModule-ec40ab0ec5fea82e970468b928afc7bf"' : 'data-target="#xs-components-links-module-LayOutModule-ec40ab0ec5fea82e970468b928afc7bf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LayOutModule-ec40ab0ec5fea82e970468b928afc7bf"' :
                                            'id="xs-components-links-module-LayOutModule-ec40ab0ec5fea82e970468b928afc7bf"' }>
                                            <li class="link">
                                                <a href="components/CabeceraComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CabeceraComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CuerpoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CuerpoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DialogoConfirmacionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DialogoConfirmacionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PieComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PieComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link">LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-ba82da1d6c622d1bf255c01a3dba34e8"' : 'data-target="#xs-components-links-module-LoginModule-ba82da1d6c622d1bf255c01a3dba34e8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-ba82da1d6c622d1bf255c01a3dba34e8"' :
                                            'id="xs-components-links-module-LoginModule-ba82da1d6c622d1bf255c01a3dba34e8"' }>
                                            <li class="link">
                                                <a href="components/LoginForbiddenComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginForbiddenComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginHomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginHomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LogoffComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogoffComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PeticionesModule.html" data-type="entity-link">PeticionesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PeticionesModule-fa3eefcff4f4dcec7030cf8da35e0dcf"' : 'data-target="#xs-components-links-module-PeticionesModule-fa3eefcff4f4dcec7030cf8da35e0dcf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PeticionesModule-fa3eefcff4f4dcec7030cf8da35e0dcf"' :
                                            'id="xs-components-links-module-PeticionesModule-fa3eefcff4f4dcec7030cf8da35e0dcf"' }>
                                            <li class="link">
                                                <a href="components/PeticionesDetalleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesDetalleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeticionesHomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesHomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeticionesListaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesListaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeticionesListaGridComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesListaGridComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeticionesListaTablaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesListaTablaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeticionesResultadoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PeticionesResultadoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiciosModule.html" data-type="entity-link">ServiciosModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/Consulta.html" data-type="entity-link">Consulta</a>
                            </li>
                            <li class="link">
                                <a href="classes/DatoGrafica.html" data-type="entity-link">DatoGrafica</a>
                            </li>
                            <li class="link">
                                <a href="classes/ItemBusqueda.html" data-type="entity-link">ItemBusqueda</a>
                            </li>
                            <li class="link">
                                <a href="classes/Peticion.html" data-type="entity-link">Peticion</a>
                            </li>
                            <li class="link">
                                <a href="classes/PeticionesJSON.html" data-type="entity-link">PeticionesJSON</a>
                            </li>
                            <li class="link">
                                <a href="classes/UsuarioTipoConsultaCount.html" data-type="entity-link">UsuarioTipoConsultaCount</a>
                            </li>
                            <li class="link">
                                <a href="classes/UsuarioTipoConsultaCountInSeries.html" data-type="entity-link">UsuarioTipoConsultaCountInSeries</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AggregationApiService.html" data-type="entity-link">AggregationApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AggregationService.html" data-type="entity-link">AggregationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PeticionApiService.html" data-type="entity-link">PeticionApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PeticionService.html" data-type="entity-link">PeticionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsuarioService.html" data-type="entity-link">UsuarioService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/TipoAggregation.html" data-type="entity-link">TipoAggregation</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});